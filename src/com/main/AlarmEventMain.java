package com.main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.dh.DpsdkCore.Alarm_Enable_Info_t;
import com.dh.DpsdkCore.Device_Info_Ex_t;
import com.dh.DpsdkCore.GetUserOrgInfo;
import com.dh.DpsdkCore.IDpsdkCore;
import com.dh.DpsdkCore.Login_Info_t;
import com.dh.DpsdkCore.Return_Value_Info_t;
import com.dh.DpsdkCore.dpsdk_alarm_type_e;
import com.dh.DpsdkCore.dpsdk_dev_type_e;
import com.dh.DpsdkCore.dpsdk_protocol_version_e;
import com.dh.DpsdkCore.dpsdk_retval_e;
import com.dh.DpsdkCore.dpsdk_sdk_type_e;
import com.dh.DpsdkCore.fDPSDKDevStatusCallback;

public class AlarmEventMain {

	public static int m_nDLLHandle = -1;

	public String m_strAlarmCamareID = "1000002"; // 报警设备ID

	public String m_strIp = "192.168.1.108"; // 登录平台ip
	public int m_nPort = 9000; // 端口
	public String m_strUser = "djf"; // 用户名
	public String m_strPassword = "12345678"; // 密码

	Return_Value_Info_t nGroupLen = new Return_Value_Info_t();

	DPSDKAlarmCallback m_AlarmCB = new DPSDKAlarmCallback();

	public fDPSDKDevStatusCallback fDeviceStatus = new fDPSDKDevStatusCallback() {
		@Override
		public void invoke(int nPDLLHandle, byte[] szDeviceId, int nStatus) {
			String status = "离线";
			if (nStatus == 1) {
				status = "在线";
				Device_Info_Ex_t deviceInfo = new Device_Info_Ex_t();
				int nRet = IDpsdkCore.DPSDK_GetDeviceInfoExById(m_nDLLHandle, szDeviceId, deviceInfo);
				if (deviceInfo.nDevType == dpsdk_dev_type_e.DEV_TYPE_NVR
						|| deviceInfo.nDevType == dpsdk_dev_type_e.DEV_TYPE_EVS
						|| deviceInfo.nDevType == dpsdk_dev_type_e.DEV_TYPE_SMART_NVR
						|| deviceInfo.nDevType == dpsdk_dev_type_e.DEV_MATRIX_NVR6000) {
					nRet = IDpsdkCore.DPSDK_QueryNVRChnlStatus(m_nDLLHandle, szDeviceId, 10 * 1000);

					if (nRet == dpsdk_retval_e.DPSDK_RET_SUCCESS) {
						// System.out.printf("查询NVR通道状态成功，deviceID = %s", new String(szDeviceId));
					} else {
						System.out.printf("查询NVR通道状态失败，deviceID = %s, nRet = %d", new String(szDeviceId), nRet);
					}
					// System.out.println();
				}
			}
			// System.out.printf("Device Status Report!, szDeviceId = %s, nStatus = %s", new
			// String(szDeviceId),status);
			// System.out.println();
		}
	};

	private void onCreate() {
		int nRet = -1;
		Return_Value_Info_t res = new Return_Value_Info_t();
		nRet = IDpsdkCore.DPSDK_Create(dpsdk_sdk_type_e.DPSDK_CORE_SDK_SERVER, res);

		m_nDLLHandle = res.nReturnValue;
		String dpsdklog = "E:\\alarmEvent\\dpsdkjavalog";
		nRet = IDpsdkCore.DPSDK_SetLog(m_nDLLHandle, dpsdklog.getBytes());
		String dumpfile = "E:\\alarmEvent\\\\dpsdkjavadump";
		nRet = IDpsdkCore.DPSDK_StartMonitor(m_nDLLHandle, dumpfile.getBytes());
		if (m_nDLLHandle > 0) {
			// 设置设备状态上报监听函数
			nRet = IDpsdkCore.DPSDK_SetDPSDKDeviceStatusCallback(m_nDLLHandle, fDeviceStatus);
		}
		System.out.print("创建DPSDK, 返回 m_nDLLHandle = ");
		System.out.println(m_nDLLHandle);
	}

	/*
	 * 登录
	 */
	public void OnLogin() {
		Login_Info_t loginInfo = new Login_Info_t();
		loginInfo.szIp = m_strIp.getBytes();
		loginInfo.nPort = m_nPort;
		loginInfo.szUsername = m_strUser.getBytes();
		loginInfo.szPassword = m_strPassword.getBytes();
		loginInfo.nProtocol = dpsdk_protocol_version_e.DPSDK_PROTOCOL_VERSION_II;
		loginInfo.iType = 1;

		int nRet = IDpsdkCore.DPSDK_Login(m_nDLLHandle, loginInfo, 10000);
		if (nRet == dpsdk_retval_e.DPSDK_RET_SUCCESS) {
			System.out.printf("登录成功，nRet = %d", nRet);
		} else {
			System.out.printf("登录失败，nRet = %d", nRet);
		}
		System.out.println();
	}

	/*
	 * 加载所有组织树
	 */
	public void LoadAllGroup() {
		int nRet = IDpsdkCore.DPSDK_LoadDGroupInfo(m_nDLLHandle, nGroupLen, 180000);

		if (nRet == dpsdk_retval_e.DPSDK_RET_SUCCESS) {
			System.out.printf("加载所有组织树成功，nRet = %d， nDepCount = %d", nRet, nGroupLen.nReturnValue);
		} else {
			System.out.printf("加载所有组织树失败，nRet = %d", nRet);
		}
		System.out.println();
	}

	/*
	 * 获取所有组织树串
	 * */
	public void GetGroupStr()
	{
		byte[] szGroupBuf = new byte[nGroupLen.nReturnValue];
		int nRet = IDpsdkCore.DPSDK_GetDGroupStr(m_nDLLHandle, szGroupBuf, nGroupLen.nReturnValue, 10000);
		
		if(nRet == dpsdk_retval_e.DPSDK_RET_SUCCESS)
		{
			String GroupBuf = "";
			try {
				GroupBuf = new String(szGroupBuf, "UTF-8");
			} catch (IOException e) {  
            e.printStackTrace();  
			} 
			System.out.printf("获取所有组织树串成功，nRet = %d， szGroupBuf = [%s]", nRet, GroupBuf);
			try {
				File file = new File("E:\\text.xml");
				if(!file.exists())
				{
					file.createNewFile();	
				}
				
				FileOutputStream  out = new FileOutputStream(file);
				out.write(szGroupBuf);
				out.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}else
		{
			System.out.printf("获取所有组织树串失败，nRet = %d", nRet);
		}
		System.out.println();
	}
	
	/*
	 * 获取用户组织信息
	 * */
	public void GetUserOrgInfo()
	{
		GetUserOrgInfo userOrgInfo = new GetUserOrgInfo();
		int nRet = IDpsdkCore.DPSDK_GetUserOrgInfo(m_nDLLHandle, userOrgInfo, 10000 );
		
		if(nRet == dpsdk_retval_e.DPSDK_RET_SUCCESS)
		{
			System.out.printf("获取用户组织信息成功，nRet = %d， UserOrgInfo = %s", nRet, userOrgInfo.strUserOrgInfo);
		}else
		{
			System.out.printf("获取用户组织信息失败，nRet = %d", nRet);
		}
		System.out.println();
	}
	
	/*
	 * 获取Ftp信息
	 * */
	public void GetFtpInfo()
	{
		byte[] szFtpInfo = new byte[64];
		int nRet = IDpsdkCore.DPSDK_GetFTPInfo(m_nDLLHandle, szFtpInfo, 64);
		
		if(nRet == dpsdk_retval_e.DPSDK_RET_SUCCESS)
		{
			String FtpInfo = "";
			try {
				FtpInfo = new String(szFtpInfo, "UTF-8");
			} catch (IOException e) {  
            e.printStackTrace();  
			} 
			System.out.printf("获取Ftp信息成功，nRet = %d， szFtpInfo = [%s]", nRet, FtpInfo);
		}else
		{
			System.out.printf("获取Ftp信息失败，nRet = %d", nRet);
		}
		System.out.println();
	}
	
	/*
	 * 布控
	 */
	public void EnableAlarm() {
		int nRet = IDpsdkCore.DPSDK_SetDPSDKAlarmCallback(m_nDLLHandle,m_AlarmCB);//设置报警监听函数
		if(nRet == dpsdk_retval_e.DPSDK_RET_SUCCESS)
		{
			System.out.printf("开启报警监听成功，nRet = %d", nRet);
		}else
		{
			System.out.printf("开启报警监听失败，nRet = %d", nRet);
		}
		System.out.println();
		
		//如若想监控多类型报警信息 需为每个类型新建结构体 每个结构体填一个布控信息
		Alarm_Enable_Info_t alarmInfo = new Alarm_Enable_Info_t(1);
		alarmInfo.sources[0].szAlarmDevId = m_strAlarmCamareID.getBytes();
		alarmInfo.sources[0].nVideoNo = -1;
		alarmInfo.sources[0].nAlarmInput = -1;
		alarmInfo.sources[0].nAlarmType = dpsdk_alarm_type_e.DPSDK_CORE_ALARM_TYPE_EXTERNAL_ALARM;
		int nRets = IDpsdkCore.DPSDK_EnableAlarm(m_nDLLHandle, alarmInfo, 10000);

		if (nRet == dpsdk_retval_e.DPSDK_RET_SUCCESS) {
			System.out.printf("报警布控成功，nRet = %d", nRets);
		} else {
			System.out.printf("报警布控失败，nRet = %d", nRets);
		}
		System.out.println();
	}
	
	/*
	 * 撤控
	 */
	public void DisableAlarm() {
		int nRet = IDpsdkCore.DPSDK_DisableAlarm(m_nDLLHandle, 10000);
		if (nRet == dpsdk_retval_e.DPSDK_RET_SUCCESS) {
			System.out.printf("报警撤控成功，nRet = %d", nRet);
		} else {
			System.out.printf("报警撤控失败，nRet = %d", nRet);
		}
		System.out.println();
	}

	/*
	 * 登出
	 */
	public void OnLogout() {
		int nRet = IDpsdkCore.DPSDK_Logout(m_nDLLHandle, 10000);
		if (nRet == dpsdk_retval_e.DPSDK_RET_SUCCESS) {
			System.out.printf("登出成功，nRet = %d", nRet);
		} else {
			System.out.printf("登出失败，nRet = %d", nRet);
		}
		System.out.println();
	}

	/*
	 * 释放内存
	 */
	public void OnDestroy() {
		int nRet = IDpsdkCore.DPSDK_Destroy(m_nDLLHandle);
		if (nRet == dpsdk_retval_e.DPSDK_RET_SUCCESS) {
			System.out.printf("释放内存成功，nRet = %d", nRet);
		} else {
			System.out.printf("释放内存失败，nRet = %d", nRet);
		}
		System.out.println();
	}

	public static void main(String[] args) {
		AlarmEventMain alarmEvent = new AlarmEventMain();
		alarmEvent.onCreate(); // 初始化
		alarmEvent.OnLogin(); // 登录
		alarmEvent.LoadAllGroup(); // 加载组织结构
		alarmEvent.GetGroupStr();//获取组织结构串
		
		alarmEvent.GetUserOrgInfo();//获取用户组织信息
		
		alarmEvent.GetFtpInfo();//获取ftp信息
		

		// 加载组织结构之后，要延时5秒钟左右，等待与各服务模块取得联系
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		alarmEvent.EnableAlarm(); //报警        
		
		try {
			Thread.sleep(60000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		alarmEvent.DisableAlarm(); //报警撤控
		
		alarmEvent.OnLogout();
		alarmEvent.OnDestroy();

	}

}

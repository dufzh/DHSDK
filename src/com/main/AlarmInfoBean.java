package com.main;

public class AlarmInfoBean {
	
	private int nPDLLHandle;
	private byte[] szAlarmId;
	private int nDeviceType;
	private byte[] szCameraId;
	private byte[] szDeviceName;
	private byte[] szChannelName;
	private byte[] szCoding;
	private byte[] szMessage;
	private int nAlarmType;
	private int nEventType;
	private int nLevel;
	private long nTime;
	private byte[] pAlarmData;
	private int nAlarmDataLen;
	private byte[] pPicData;
	private int nPicDataLen;
	
	public int getnPDLLHandle() {
		return nPDLLHandle;
	}
	public void setnPDLLHandle(int nPDLLHandle) {
		this.nPDLLHandle = nPDLLHandle;
	}
	public byte[] getSzAlarmId() {
		return szAlarmId;
	}
	public void setSzAlarmId(byte[] szAlarmId) {
		this.szAlarmId = szAlarmId;
	}
	public int getnDeviceType() {
		return nDeviceType;
	}
	public void setnDeviceType(int nDeviceType) {
		this.nDeviceType = nDeviceType;
	}
	public byte[] getSzCameraId() {
		return szCameraId;
	}
	public void setSzCameraId(byte[] szCameraId) {
		this.szCameraId = szCameraId;
	}
	public byte[] getSzDeviceName() {
		return szDeviceName;
	}
	public void setSzDeviceName(byte[] szDeviceName) {
		this.szDeviceName = szDeviceName;
	}
	public byte[] getSzChannelName() {
		return szChannelName;
	}
	public void setSzChannelName(byte[] szChannelName) {
		this.szChannelName = szChannelName;
	}
	public byte[] getSzCoding() {
		return szCoding;
	}
	public void setSzCoding(byte[] szCoding) {
		this.szCoding = szCoding;
	}
	public byte[] getSzMessage() {
		return szMessage;
	}
	public void setSzMessage(byte[] szMessage) {
		this.szMessage = szMessage;
	}
	public int getnAlarmType() {
		return nAlarmType;
	}
	public void setnAlarmType(int nAlarmType) {
		this.nAlarmType = nAlarmType;
	}
	public int getnEventType() {
		return nEventType;
	}
	public void setnEventType(int nEventType) {
		this.nEventType = nEventType;
	}
	public int getnLevel() {
		return nLevel;
	}
	public void setnLevel(int nLevel) {
		this.nLevel = nLevel;
	}
	public long getnTime() {
		return nTime;
	}
	public void setnTime(long nTime) {
		this.nTime = nTime;
	}
	public byte[] getpAlarmData() {
		return pAlarmData;
	}
	public void setpAlarmData(byte[] pAlarmData) {
		this.pAlarmData = pAlarmData;
	}
	public int getnAlarmDataLen() {
		return nAlarmDataLen;
	}
	public void setnAlarmDataLen(int nAlarmDataLen) {
		this.nAlarmDataLen = nAlarmDataLen;
	}
	public byte[] getpPicData() {
		return pPicData;
	}
	public void setpPicData(byte[] pPicData) {
		this.pPicData = pPicData;
	}
	public int getnPicDataLen() {
		return nPicDataLen;
	}
	public void setnPicDataLen(int nPicDataLen) {
		this.nPicDataLen = nPicDataLen;
	}
	
	
}
